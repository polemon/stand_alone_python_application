#!/usr/bin/bash

pushd app
7z a app.zip *py
mv app.zip ..
popd

echo "#! /usr/bin/env python" > app.pyc
cat app.zip >> app.pyc
chmod +x app.pyc
