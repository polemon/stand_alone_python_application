#! /usr/bin/env python

import zipapp

zipapp.create_archive(
        source = "app/",
        target = "app.pyc",
        interpreter = "/usr/bin/env python",
        compressed = True )
